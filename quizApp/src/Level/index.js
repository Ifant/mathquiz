import React, { Component } from 'react';
import { Text,View,TouchableOpacity,StyleSheet,StatusBar,Image } from 'react-native';

export default class Level extends Component{
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor= '#ecf0f1' barStyle = 'dark-content'/>
                <TouchableOpacity>
                    <Image source={require('../image/Path1.png')} style={styles.back}/>
                </TouchableOpacity>
                <Text style={styles.title}>Select Level</Text>
                <View style={{alignItems: "center", top: 140}}>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate('OnPlay')}>
                        <Tombol title='Easy'/>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Tombol title='Medium'/>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Tombol title='Hard'/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const Tombol = (props) => {
    return(
            <View style={styles.kotak}>
                <Text style={styles.textKotak}>{props.title}</Text>
            </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3C94E0',
    },
    kotak: {
        width: 151,
        height: 61,
        backgroundColor: '#FDE95F',
        marginBottom: 20,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center"
    },
    back: {
        marginTop: 16,
        marginLeft: 20
    },
    textKotak: {
        fontSize: 16,
    },
    title: {
        fontSize: 35,
        color: 'white',
        textAlign: "center",
        top: 100,
    }
})