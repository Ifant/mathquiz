import React, { Component } from 'react';
import { Text,Image,Button,View,StatusBar,StyleSheet,TouchableOpacity } from 'react-native';


export default class Home extends Component {
    render(){
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor= '#ecf0f1' barStyle = 'dark-content'/>
                <Image source={require('../image/aritmatikaLogo.png')} style={styles.logo}/>
                <Text style={styles.title}>ARITHMATIC{'\n'} QUIZ</Text>
                <TouchableOpacity onPress={()=> this.props.navigation.navigate('Level')}>
                    <Tombol1 title='PLAY'/>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=> alert('Masih Tahap Konstruksi')}>
                    <Tombol1 title='SCORE BOARD'/>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => alert('Masih Tahap Konstruksi')}>
                    <Tombol2 title='ABOUT'/>
                </TouchableOpacity>

                <View style={styles.exit}>
                    <Button
                    title ='EXIT' 
                    color='#e74c3c' 
                    >
                    </Button>
                </View>
            </View>
        )
    }
}

const Tombol1 = (props) =>{
    return(
        <View style= {styles.kotak}>
            <Text style={styles.textKotak}>{props.title}</Text>
        </View>
    )
}

const Tombol2 = (props) =>{
    return(
        <View style= {styles.kotakAbout}>
            <Text style={styles.textKotak}>{props.title}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3C94E0',
        alignItems: "center"
    },  
    logo: {
        width: 122,
        height: 109,
        marginTop: 32,
    },
    title: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: "center",
        fontSize: 40,
        marginBottom: 60
    },
    kotak: {
        width: 151,
        height: 61,
        backgroundColor: '#FDE95F',
        marginBottom: 10,
        borderRadius: 10,
        justifyContent: "center"
    },
    textKotak: {
        fontSize: 16,
        textAlign: "center",
        fontWeight: "bold"
    },
    kotakAbout: {
        width: 151,
        height: 39,
        backgroundColor: '#FDE95F',
        borderRadius: 10,
        marginVertical: 19,
        justifyContent: "center"
    },
    exit: {
        top: 90,
        width: 87,
        height: 35,
        borderRadius: 20,
    }
})