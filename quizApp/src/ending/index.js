import React, { Component } from 'react';
import { View,Text,TouchableOpacity,StyleSheet,StatusBar } from 'react-native';

export default class ending extends Component{
    render(){
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor= '#ecf0f1' barStyle='dark-content' />
                <View style={styles.papan}>
                    <View style={styles.selamatBoard}>
                        <Text style={styles.selamatText}>SELAMAT</Text>
                    </View>
                    <View style={styles.papanSkor}>
                        <Text style={styles.skorTextTitle}>SKOR</Text>
                        <Text style={styles.skor}>80</Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent:'space-evenly'}}>
                        <View style={styles.bottomScore}>
                            <Text style={{color:'#FDE95F',textAlign:'center'}}>Best Score</Text>
                            <Text style={{color:'white',textAlign:'center'}}>90</Text>
                        </View>
                        <View style={styles.bottomScore}>
                            <Text style={{color:'#FDE95F',textAlign:'center'}}>Rangking</Text>
                            <Text style={{color:'white',textAlign:"center"}}>5</Text>
                        </View>     
                    </View>
                </View>
                <TouchableOpacity>
                    <View style={styles.button}>
                        <Text style={{fontWeight:'bold'}}>PLAY AGAIN</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity>
                    <View style={styles.button}>
                        <Text style={{fontWeight:'bold'}}>MENU</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
} 

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#3C94E0',
        flex: 1
    },
    papan: {
        width: 300,
        height: 261,
        backgroundColor: '#E74C3C',
        marginTop: 94,
        marginLeft: 'auto',
        marginRight: 'auto',
        borderRadius: 20,
        borderColor: 'white',
        borderWidth: 4,
        marginBottom: 100
    },
    selamatBoard: {
        width: 145,
        height: 44,
        top: -22,
        backgroundColor: '#27AE60',
        borderRadius: 20,
        borderWidth: 4,
        borderColor: 'white',
        justifyContent: "center",
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    selamatText: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        textAlign: "center"
    },
    papanSkor: {
        width: 220,
        height: 124,
        backgroundColor: '#27AE60',
        borderRadius: 20,
        marginLeft: 'auto',
        marginRight: 'auto'

    },
    skorTextTitle: {
        fontSize: 15,
        color: '#FDE95F',
        fontWeight: "bold",
        textAlign: "center"
    },
    skor: {
        fontSize: 40,
        fontWeight: "bold",
        color: 'white',
        textAlign: "center",
        top: 14
    },
    bottomScore: {
        marginTop: 15,
        width: 88,
        height: 44,
        borderRadius: 10,
        backgroundColor: '#27AE60',
    },
    button: {
        width: 190,
        height: 40,
        backgroundColor: '#FDE95F',
        borderRadius: 20,
        marginLeft:'auto',
        marginRight:'auto',
        marginBottom:15,
        alignItems: 'center',
        justifyContent:'center'
    }
})