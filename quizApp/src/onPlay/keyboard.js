import React, { Component } from 'react';
import { Text,View,TouchableOpacity,StyleSheet } from 'react-native';

export default class Keyboard extends Component {
    render() {
        return(
            <View style={styles.container}>
                <View style={styles.rows}> 
                    <Kotak title='1' aksi={this.props.tambah1}/>
                    <Kotak title='2'aksi={this.props.tambah2}/>
                    <Kotak title='3' aksi={this.props.tambah3}/>
                </View>
                <View style={styles.rows}>
                    <Kotak title='4' aksi={this.props.tambah4}/>
                    <Kotak title='5' aksi={this.props.tambah5}/>
                    <Kotak title='6' aksi={this.props.tambah6}/>
                </View>
                <View style={styles.rows}>
                    <Kotak title='7' aksi={this.props.tambah7}/>
                    <Kotak title='8' aksi={this.props.tambah8}/>
                    <Kotak title='9' aksi={this.props.tambah9}/> 
                </View>
                <View style={styles.rows}>
                    <Kotak title='-' aksi={this.props.tambahMin}/>
                    <Kotak title='0' aksi={this.props.tambah0}/>
                    <Kotak title='DEL' aksi={this.props.delete}/>
                </View>
            </View>
        )
    }

}

const Kotak = (props) => {
    return(
        <TouchableOpacity onPress={props.aksi}>
            <View style={styles.kotak}>
                <Text style={styles.angka}>{props.title}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    kotak: {
        width: 54,
        height: 44,
        borderRadius: 10,
        backgroundColor: '#676262',
        alignItems: "center",
        justifyContent: "center",
        margin: 5
    },
    angka: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold'
    },
    container: {
        top: 14,
        justifyContent: "center",
        alignItems: "center"
    },
    rows: {
        flexDirection: "row"
    },
})
