import React, { Component } from 'react';
import { Text,Image,TouchableOpacity,View,StyleSheet,StatusBar,ScrollView } from 'react-native';
import Keyboard from './keyboard';

export default class OnPlay extends Component{
    state = {
        jawaban:'',
        soalKiri: 99,
        soalKanan: 99,
        betul: false
    }
    
    componentDidMount() {
        this.setState( {soalKiri: Math.floor(Math.random() * 20),soalKanan: Math.floor(Math.random() * 20)} )
    }
    
    submitHandler() {
        let jawabanInt = parseInt(this.state.jawaban);
        let correct = this.state.soalKiri + this.state.soalKanan;
        if (jawabanInt===correct){
            this.setState({betul: true})
        } 
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor= '#ecf0f1' barStyle='dark-content' />
                <ScrollView>
                    <View style={ styles.header }>
                        <View style={styles.boxSoal}>
                            <Text style={ styles.textNoSoal}>SOAL 1</Text>
                        </View>
                        <View style={ styles.menuLogo }>
                            <TouchableOpacity>
                                <View style={styles.bar}/>
                                <View style={styles.bar}/>
                                <View style={styles.bar}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={ styles.timer }>
                        <Text style={styles.textTimer}>10</Text>
                    </View>
                    <View style={styles.whiteBoard}>
                        <Text style={styles.textSoal}>{this.state.soalKiri} + {this.state.soalKanan} =</Text>
                    </View>
                    <View style={{flexDirection: "row",justifyContent: "center", alignItems: "center",marginTop: 25}}>
                        <Text style={{fontSize:26,color:'white',fontWeight:'bold'}}>Answer</Text>
                        <View style={styles.boxJawab}>
                            <Text style={{ fontSize:25,fontWeight:"bold",textDecorationLine: "underline"}}>{this.state.jawaban}</Text>
                        </View>
                    </View>
                    <View style={this.state.betul ? styles.notifBack: styles.notifBackHidden}>
                        <Text style={this.state.betul ? styles.notif: styles.notifFalse}>JAWABAN ANDA BENAR!</Text>
                    </View>
                    <Keyboard style={this.state.betul ? styles.keyboardHidden: styles.keyboardShow}
                    navigasi={() => this.props.navigation.navigate('Ending')}
                    tambah1= { () =>this.tambah(1) }
                    tambah2= { () =>this.tambah(2) }
                    tambah3={ () =>this.tambah(3) }
                    tambah4={ () =>this.tambah(4) }
                    tambah5={ () =>this.tambah(5) }
                    tambah6={ () =>this.tambah(6) }
                    tambah7={ () =>this.tambah(7) }
                    tambah8={ () =>this.tambah(8) }
                    tambah9={ () =>this.tambah(9) }
                    tambah0={ () =>this.tambah(0) }
                    tambahMin={ () =>this.tambah('-')}
                    delete= { () => this.setState({ jawaban: this.state.jawaban.substr(0,this.state.jawaban.length-1)})}
                    />
                    <TouchableOpacity onPress={() => this.submitHandler()}>
                        <View style={styles.submit}>
                            <Text style={{fontSize: 20, fontWeight: 'bold',color: '#676262'}}>SUBMIT</Text>
                        </View>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        )
    }

    tambah(a) {
        if (a==='-'){
            if (this.state.jawaban.length === 0){
                this.setState({jawaban: this.state.jawaban + a})
            }
        }else {
            if (this.state.jawaban.length<=6){
                this.setState({jawaban: this.state.jawaban + a})
            }
        }
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3C94E0',
    },
    boxSoal: {
        width: 120,
        height: 40,
        backgroundColor: '#E74C3C',
        borderTopStartRadius: 20,
        borderBottomRightRadius: 20,
        justifyContent: "center",
        marginLeft: 10
    },
    textNoSoal: {
        fontSize: 25,
        textAlign: "center",
        fontWeight: "bold",
        color: 'white'
    },
    menuLogo: {
        width: 50,
        height: 50,
        backgroundColor: 'white',
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        marginRight: 10
    },
    bar: {
        width: 24,
        height: 5,
        backgroundColor: 'black',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        marginBottom: 4,
    },
    header: {
        flexDirection: 'row', 
        justifyContent: 'space-between',
        alignItems: "center",
        top: 15
    },
    timer: {
        width: 50,
        height: 50,
        backgroundColor: 'white',
        borderRadius: 25,
        marginTop: 15,
        marginLeft: 'auto',
        marginRight: 'auto',
        justifyContent: "center",
    },
    textTimer: {
        fontSize: 30,
        fontWeight: "bold",
        color: '#FFA2A2',
        textAlign: "center"
    },
    whiteBoard: {
        width: 299,
        height: 182,
        top: 10,
        marginLeft: 'auto',
        marginRight: 'auto',
        backgroundColor: 'white',
        borderWidth: 9,
        borderColor: '#FFA2A2',
        justifyContent: "center"
    },
    textSoal: {
        fontSize: 44,
        textAlign: "center"
    },
    boxJawab: {
        width: 148,
        height: 50,
        backgroundColor: '#FFA2A2',
        borderRadius: 60,
        marginLeft: 30,
        justifyContent: "center",
        alignItems: "center"
    },
    notif: {
        textAlign: 'center',
        fontSize: 20,
        color:'white',
        fontSize: 20,
        color:'white',

    },
    notifFalse: {
        fontSize: 20,
        color:'transparent'
    },
    notifBack: {
        backgroundColor: '#9b59b6',
        width: 300,
        height: 50,
        marginTop: 20,
        marginLeft: 'auto',
        marginRight: 'auto',
        justifyContent:'center',
        borderRadius: 30
    },
    notifBackHidden: {
        backgroundColor: 'transparent',
        position: 'absolute',
        width: 300,
        height: 50,
        justifyContent:'center'
    },
    submit: {
        width: 110,
        height: 35,
        backgroundColor: 'white',
        marginTop: 20,
        marginRight: 'auto',
        marginLeft: 'auto',
        borderRadius: 15,
        justifyContent: "center",
        alignItems: "center"
    },
    keyboardShow: {
    },
    keyboardHidden: {
        color: 'transparent'
    }

})